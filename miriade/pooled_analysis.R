##################################################
## Project: MIRIADE
## Script purpose: Integrate different biomarker sources in MIRIADE and annotate them with stable IDs
## Date: 07.01.2022
## Author: Marek Ostaszewski
##################################################

options(stringsAsFactors = F)

library(dplyr)

### Create a reference table of all datasets to read in
dat_tables_to_read <- rbind(
  c(path = "_notgit/cleaned_VUMC_olink.tsv", source = "VUMC Olink"),
  c(path = "_notgit/cleaned_VUMC_mspec.tsv", source = "VUMC MSpec"),
  c(path = "_notgit/cleaned_kth_entries.tsv", source = "KTH"),
  c(path = "_notgit/cleaned_adni_entries.tsv", source = "ADNI"),
  c(path = "_notgit/cleaned_emif_entries.tsv", source = "EMIF"),
  c(path = "_notgit/cleaned_bPRIDE_entries.tsv", source = "bPRIDE")
) %>% as.data.frame()

### Select a list of common columns for integration
common_cols <- c("UniProt", "p.val", "disease")

### Pool all the entries together, add source information
### using UniProt as the index column
all_combined <- purrr::pmap(dat_tables_to_read,
                            ~ readr::read_tsv(..1) %>% 
                              dplyr::select(!!!common_cols) %>%
                              dplyr::mutate(source = ..2)) %>%
  purrr::reduce(rbind)


### Combine p values, thanks to Armin Rauschenberger 
### for explaining and implementing the Fisher and Simes  method
fisher_method <- function(pval) {
  1-stats::pchisq(q  = sum(-2*log(pval[!is.na(pval)])),
                  df = 2*sum(!is.na(pval)))
}

dplyr::filter(all_combined, UniProt == "A0A075B6K4")

library(palasso)
### Calculate for both Fisher and Simes, create adjusted p values
combined <- dplyr::group_by(all_combined, disease, UniProt) %>% 
  dplyr::summarise(p.val_fisher = fisher_method(p.val), 
                   p.val_simes = palasso::.combine(p.val, method = "simes"),
                   sources = paste(sort(source), collapse = ";")) %>%
  dplyr::ungroup() %>% dplyr::group_by(disease) %>%
  dplyr::mutate(adj.p.val_fisher = p.adjust(p.val_fisher, method = "BY"), 
                adj.p.val_simes = p.adjust(p.val_simes, method = "BY"))

### Tables of disease-specific entries 
ALZ_df <- dplyr::select(combined, UniProt, adj.p.val_simes, disease, sources) %>%
  dplyr::filter(disease == "ALZ") %>%
  dplyr::rename(adj.p = adj.p.val_simes) %>%
  dplyr::filter(adj.p <= 0.1) %>% distinct()
  
FTD_df <- dplyr::select(combined, UniProt, adj.p.val_simes, disease, sources) %>%
  dplyr::filter(disease == "FTD") %>%
  dplyr::rename(adj.p = adj.p.val_simes) %>%
  dplyr::filter(adj.p <= 0.1) %>% distinct()

DLB_df <- dplyr::select(combined, UniProt, adj.p.val_simes, disease, sources) %>%
  dplyr::filter(disease == "DLB") %>%
  dplyr::rename(adj.p = adj.p.val_simes) %>%
  dplyr::filter(adj.p <= 0.1) %>% distinct()

### Create a table of proteins specific for each of the tables
specific_BMs <- rbind(
  dplyr::filter(ALZ_df, UniProt %in% setdiff(ALZ_df$UniProt, c(FTD_df$UniProt, DLB_df$UniProt))),
  dplyr::filter(FTD_df, UniProt %in% setdiff(FTD_df$UniProt, c(ALZ_df$UniProt, DLB_df$UniProt))),
  dplyr::filter(DLB_df, UniProt %in% setdiff(DLB_df$UniProt, c(FTD_df$UniProt, ALZ_df$UniProt)))
  )

### Create a table of proteins common between the tables
common_BMs <- rbind(
  dplyr::filter(ALZ_df, UniProt %in% union(FTD_df$UniProt, DLB_df$UniProt)),
  dplyr::filter(FTD_df, UniProt %in% union(ALZ_df$UniProt, DLB_df$UniProt)),
  dplyr::filter(DLB_df, UniProt %in% union(FTD_df$UniProt, ALZ_df$UniProt))
) 
### Cleanup the common table by merging same UniProt entries
common_BMs <- dplyr::group_by(common_BMs, UniProt) %>% 
  dplyr::summarise(adj.p = min(adj.p), 
                   sources = paste(paste0(disease,":",sources), collapse = ";"),
                   disease = paste(disease, collapse = ";"))

### Create a common table of candidate proteins to be used downstream 
candidate_BMs <- rbind(specific_BMs, common_BMs)

### Gene names and Entrez ids, for downstream data analysis/enrichment
### Retrieve HGNCs and NCBI gene ids from AnnotationDbi, using UniProt ids
library(AnnotationDbi)
library(org.Hs.eg.db)
id_mapping <- AnnotationDbi::select(org.Hs.eg.db, candidate_BMs$UniProt, 
                                    columns = c("SYMBOL", "ENTREZID"), keytype = "UNIPROT")
### Manual tweaks to the mapping, there are some inconsistencies
id_mapping[id_mapping$UNIPROT == "Q8WWJ7", 2:3] <- c("EPHB6", "2051")
id_mapping[id_mapping$UNIPROT == "Q8N423", 2:3] <- c("LILRB2", "10288")
id_mapping[id_mapping$UNIPROT == "Q8WWJ7", 2:3] <- c("ACAN", "176")
id_mapping <- dplyr::filter(id_mapping, !(SYMBOL %in% c("TNFSF12-TNFSF13", "C4B_2", "CALM2", "CALM3")))

### Create the annotated set of biomarkers
annotated_BMs <- merge(candidate_BMs, id_mapping, by.x = "UniProt", by.y = "UNIPROT")
write.table(annotated_BMs, file = "_notgit/annotated_candidate_biomarkers.tsv", 
            sep = "\t", quote = F, row.names = F, col.names = T)