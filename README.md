# BioData Analysis

A repository to store code used in various projects for data cleanup, integration or analysis.

Projects so far:
- MIRIADE dataset cleanup and analysis
- MINERVA ovarlay hit calculation (using the PD map as an example)
- ExpoBiome Map construction [to be uploaded]
- NaviCell comment handling for ACSN purposes [to be uploaded]
- others 
